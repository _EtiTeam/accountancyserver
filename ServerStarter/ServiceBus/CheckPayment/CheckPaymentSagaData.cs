﻿using NServiceBus;
using System;
using System.Collections.Generic;

namespace AccountancyServerStarter.ServiceBus.CheckPayment
{
    class CheckPaymentSagaData : ContainSagaData
    {
        public CheckPaymentSagaData()
        {
            OrderSagaIds = new HashSet<Guid>();
        }

        public int OrderId { get; set; }

        public HashSet<Guid> OrderSagaIds { get; set; }
    }
}
