﻿using NServiceBus;
using NServiceBus.Logging;
using ServiceBusDLL;
using System;
using System.Threading.Tasks;
using NServiceBus.Persistence.Sql;
using System.Text;
using System.Collections.Generic;

namespace AccountancyServerStarter.ServiceBus.CheckPayment
{
    class CheckPaymentSaga :
        SqlSaga<CheckPaymentSagaData>,
        IAmStartedByMessages<CheckOrderPayment>,
        IHandleMessages<OrderPaid>
    {
        static ILog log = LogManager.GetLogger<CheckPaymentSaga>();

        protected override string CorrelationPropertyName => nameof(Data.OrderId);

        protected override void ConfigureMapping(IMessagePropertyMapper mapper)
        {
            mapper.ConfigureMapping<CheckOrderPayment>(message => message.OrderId);
            mapper.ConfigureMapping<OrderPaid>(message => message.OrderId);
        }
        
        public Task Handle(CheckOrderPayment message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.OrderId } ||| Fetched CheckOrderPayment: { message.Id }");

            if (Data.OrderSagaIds.Equals(Guid.Empty))
            {
                log.Error("Fetech request with OrderSagaId==Guid.Empty. Message has been ignored");
                return Task.CompletedTask;
            }
            if (!Data.OrderSagaIds.Contains(message.Id) && Data.OrderSagaIds.Count > 0)
            {
                log.Warn("Second request for the same orderId");
            }
            Data.OrderSagaIds.Add(message.Id);

            //Data.OrderId = message.OrderId; // it is not required
            // return RequestTimeout<SimpleTimeout>(context, TimeSpan.FromSeconds(1));
            return Task.CompletedTask;
        }

        public Task Handle(OrderPaid message, IMessageHandlerContext context)
        {
            log.Info($"Saga: { Data.OrderId } " +
                $"||| Fetched OrderPaid: { message.OrderId } " +
                $"||| Response to: { serializeList(Data.OrderSagaIds) }"
                );
            foreach (var orderSagaId in Data.OrderSagaIds)
            {
                context.Send(Data.Originator, new CheckOrderPaymentResponse { Id = orderSagaId });
            }
            MarkAsComplete();
            return Task.CompletedTask;
        }


        private static string serializeList<T>(ICollection<T> collection)
        {
            StringBuilder b = new StringBuilder();
            b.Append("[");
            foreach (var i in collection)
                b.Append(i.ToString()).Append(",");
            b.Remove(b.Length - 1, 1);
            b.Append("]");
            return b.ToString();
        }
    }
}
