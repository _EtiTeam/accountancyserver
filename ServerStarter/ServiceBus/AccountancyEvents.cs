﻿using NServiceBus;
using ServiceBusDLL;

namespace AccountancyServerStarter.ServiceBus
{
    static class AccountancyEvents
    {
        private static IEndpointInstance busEndpoint;
        public static void initialize(IEndpointInstance endpoint) // it also call static constructor
        {
            busEndpoint = endpoint;
        }

        static AccountancyEvents()
        {
            // set event as action in delegates
            AccountancyService.AccountancyDelegates.AddOnOrderPaidAction(onOrderPaid);
        }

        public static void onOrderPaid(int orderId)
        {
            busEndpoint.SendLocal<OrderPaid>(x => x.OrderId = orderId);
        }
    }
}
