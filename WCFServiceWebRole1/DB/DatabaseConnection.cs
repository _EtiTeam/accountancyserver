﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ModelDLL;

namespace AccountancyService
{
    
    public class DatabaseConnection : ModelDLL.IDatabaseConnection
    {
        public SqlConnection SqlConnection => getConnection();

        private static SqlConnection getConnection()
        {
            SqlConnection c = new SqlConnection();
            c.ConnectionString = Properties.Settings.Default.ConnectionString; // from Properties/Settings.settings
            return c;
        }
    }
}