﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace AccountancyService
{
    [ServiceContract]
    public interface IWebAccountancyService
    {
        [OperationContract]
        [WebGet(UriTemplate = "")]
        void redirection();
    }
}
