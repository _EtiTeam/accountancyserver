﻿using System;
using System.Data.SqlClient;

namespace AccountancyService
{
    public class UserService : UserServiceDll.UserServiceImpl
    {
        public override SqlConnection SqlConnection =>
            new DatabaseConnection().SqlConnection;

        public override string baseGuiPath => 
            ServerConfiguration.basePath;
    }
}
