﻿using ModelDLL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.ServiceModel.Web;
using System.Text;

namespace AccountancyService
{
    public class AccountancyService : IWebAccountancyService, IRestAccountancyService
    {
        public static void initialize()
        {
            new AccountancyService();
        }

        public AccountancyService()
        {
            createTablesIfNotExist();
        }

        private void createTablesIfNotExist()
        {
            if (ModelOperations.CheckIfTableExists(Payment.TableName, new DatabaseConnection()) == false)
            {
                recreateTables();
            }
        }

        public void redirection()
        {
            var ctx = WebOperationContext.Current.OutgoingResponse;
            ctx.Location = "rest/index.html";
            ctx.StatusCode = System.Net.HttpStatusCode.Redirect;
        }

        public void recreateTables()
        {
            //drop tables
            ModelOperations.DropTableIfExists(Payment.TableName, new DatabaseConnection());           

            //create tables
            ModelOperations.ExecuteSqlCommand(Payment.CreateCommand, new DatabaseConnection());
            
            //insert mock values into tables
            ModelOperations.ExecuteSqlCommand(Payment.mockValuesCommand, new DatabaseConnection());

            ModelOperations.ExecuteSqlCommand("DELETE FROM AccountancyService_CheckPaymentSaga", new ServiceBusDLL.SagasDatabaseConnection());
        }

        private bool isAuthentificated()
        {
            bool isAuthentificated = false;
            WebOperationContext webOperationContext = WebOperationContext.Current;
            if (webOperationContext != null)
            {
                var sessionUUID = webOperationContext.IncomingRequest.Headers.Get("Authorization");

                if (sessionUUID != null)
                {

                    sessionUUID = sessionUUID.Replace("\\", "").Replace("\"", "");
                    try
                    {
                        var userService = UserServiceConnector.createInstance();
                        using ((System.IDisposable)userService)
                        {
                            isAuthentificated = userService.isSignedIn(Guid.Parse(sessionUUID));
                        }
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }

            }
            return isAuthentificated;

        }

        public Stream getLoginPage()
        {
            return getStreamFromFile(ServerConfiguration.basePath + "login.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        public Stream getJsFile(string jsFileName)
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\js\\" + jsFileName, GuiDLL.GuiConstants.JS_TYPE);
        }

        public Stream getCssFile(string cssFile)
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\css\\" + cssFile, GuiDLL.GuiConstants.CSS_TYPE);
        }

        public Stream getImageFile(string imageFile)
        {
            return getStreamFromFile(ServerConfiguration.basePath + "images\\" + imageFile, GuiDLL.GuiConstants.IMG_TYPE);
        }

        public Stream getFont1()
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\fonts\\fontawesome-webfont.woff", "application/font-woff");
        }

        public Stream getFont2()
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\fonts\\fontawesome-webfont.woff2", "application/font-woff2");
        }

        public Stream getFont3()
        {
            return getStreamFromFile(ServerConfiguration.basePath + "assets\\fonts\\fontawesome-webfont.ttf", "application/x-font-ttf");
        }

        public Stream getIndexPage()
        {
            if (!isAuthentificated())
                return getLoginPage();
            else
                return getStreamFromFile(ServerConfiguration.basePath + "index.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        public Stream getSubPage()
        {
            if (!isAuthentificated())
                return getLoginPage();
            else
                return getStreamFromFile(ServerConfiguration.basePath + "subpage.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        private static Stream getStreamFromFile(string filePath, string contentType)
        {
            var ctx = WebOperationContext.Current.IncomingRequest;
            
            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + ctx.UriTemplateMatch.RequestUri.ToString());
            OutgoingWebResponseContext context =
                WebOperationContext.Current.OutgoingResponse;
            context.ContentType = contentType;
            try
            {
                byte[] file = File.ReadAllBytes(filePath);
                return new MemoryStream(file);
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                return new MemoryStream(ASCIIEncoding.UTF8.GetBytes($"File not found. Description: {fileNotFoundException.Message}"));
            }
        }

        public IList<ModelDLL.Payment> getPaymentSelect()
        {
            if (!isAuthentificated())
                return null;
            else
                return ModelOperations.Select<Payment>(Payment.SelectCommand, new DatabaseConnection());           
        }               
       
        public void postPaymentInsert(System.IO.Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                Payment payment = ModelOperations.GetFromJson<Payment>(s).First();

                try
                {
                    ModelOperations.Insert(payment, new DatabaseConnection());
                    AccountancyDelegates.InvokeOnOrderPaid(payment.OrderID);
                }
                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }

        }

        public void postPaymentUpdate(System.IO.Stream s)
        {
            if (!isAuthentificated())
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return;
            }
            else
            {
                Payment payment = ModelOperations.GetFromJson<Payment>(s).First();

                try
                {
                    ModelOperations.Update(payment, new DatabaseConnection());
                    AccountancyDelegates.InvokeOnOrderPaid(payment.OrderID);
                }

                catch (SqlException ex)
                {
                    WebOperationContext ctx = WebOperationContext.Current;
                    ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                    ctx.OutgoingResponse.StatusDescription = ex.Message;
                }
            }
        }

        public void getTableDelete(string tableName, string id)
        {
            if (!isAuthentificated())
            {
                return;
            }
            try
            {
                ModelOperations.ExecuteSqlCommand($"delete from {tableName} where ID='{id}'", new DatabaseConnection());

            }
            catch (SqlException ex)
            {
                WebOperationContext ctx = WebOperationContext.Current;
                ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Conflict;
                ctx.OutgoingResponse.StatusDescription = ex.Message;
            }
        }
    }
}
