﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace WCFSalesService
{
    [ServiceContract]
    public interface ISalesService
    {
        [OperationContract]
        [WebGet(UriTemplate = "login.html")]
        string getIndex();

        [OperationContract]
        void recreateTables();
    }
}
