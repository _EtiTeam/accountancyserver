﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccountancyService
{
    public static class AccountancyDelegates
    {
        private static volatile Action<int> onOrderPaid = x => { }; // empty delegate

        public static void InvokeOnOrderPaid(int orderId)
        {
            lock (onOrderPaid)
            {
                onOrderPaid(orderId);
            }
        }

        public static void AddOnOrderPaidAction(Action<int> method)
        {
            lock (onOrderPaid)
            {
                onOrderPaid += method;
            }
        }

        public static void RemoveOnOrderPaidAction(Action<int> method)
        {
            lock (onOrderPaid)
            {
                onOrderPaid -= method;
            }
        }
    }
}