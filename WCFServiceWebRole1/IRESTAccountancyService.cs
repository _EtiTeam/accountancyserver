﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace AccountancyService
{
    [ServiceContract]
    public interface IRestAccountancyService
    {
        [OperationContract]
        [WebGet(
            UriTemplate = "Payments/select",
            ResponseFormat = WebMessageFormat.Json
        )]
        IList<ModelDLL.Payment> getPaymentSelect();

        [OperationContract]
        [WebInvoke(
           UriTemplate = "Payments/insert",
           ResponseFormat = WebMessageFormat.Json
       )]
        void postPaymentInsert(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(
           UriTemplate = "Payments/update",
           ResponseFormat = WebMessageFormat.Json
       )]
        void postPaymentUpdate(System.IO.Stream s);


        [OperationContract]
        [WebGet(
            UriTemplate = "{tableName}/delete/{id}",
            ResponseFormat = WebMessageFormat.Json
        )]
        void getTableDelete(string tableName, string id);

        [OperationContract]
        [WebGet(UriTemplate = "rt")]
        void recreateTables();




        //Main Web Resources 
        [OperationContract]
        [WebGet(UriTemplate = "assets/js/{jsFile}")]
        System.IO.Stream getJsFile(string jsFile);

        [OperationContract]
        [WebGet(UriTemplate = "assets/css/{cssFile}")]
        System.IO.Stream getCssFile(string cssFile);

        [OperationContract]
        [WebGet(UriTemplate = "images/{imageFile}")]
        System.IO.Stream getImageFile(string imageFile);

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.woff?v=4.6.3")]
        System.IO.Stream getFont1();

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.woff2?v=4.6.3")]
        System.IO.Stream getFont2();

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.ttf?v=4.6.3")]
        System.IO.Stream getFont3();

        //Accountancy Web Resources
        [OperationContract]
        [WebGet(UriTemplate = "index.html")]
        System.IO.Stream getIndexPage();

        [OperationContract]
        [WebGet(UriTemplate = "subpage.html")]
        System.IO.Stream getSubPage();
    }
}
